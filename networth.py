#!/usr/bin/python3

# Copyright 2017-2018 Peter Rhone 
#
# NetWorth is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License 
# as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# NetWorth is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Foobar. 
# If not, see http://www.gnu.org/licenses/.
# 
# This is V2 of the file, modified to comply with V2 of the coinmarketcap api

# USAGE:
# This file must be modified for it to be useful to you. Search Tag CUSTOMIZE to find the relevant areas
# 3rd Party libraries are required, and can be installed with
# pip3 install termcolor
# pip3 install terminaltables

import requests
import json
from decimal import *
from termcolor import colored
from terminaltables import SingleTable

TWOPLACES = Decimal('0.01')

# https://btcdiv.com/ check various ownerships
#CUSTOMIZE by adding your coins - names are from coinmarketcap.com, 
#any currency they support may be used,the number beside the coin 
#represents the amount you own
#NOTE: if you have coins spread out, you can add the amounts in the tokens list, 
#for an exmaple look at the bitcoin entry

#the relevant ethereum tokens can be seen here: https://etherscan.io/address/0x44196d12f48b8e04533c9988e2eb81f83bbd7343

tokens={'bitcoin':0.5+0.5,
        'bitcoin-cash':2,
        'ethereum':3,
        'dash':4,
        'stellar':5,
        'bitcoin-diamond':6,
        'viuly':7,
        'xenon':8}

#this dictionary contains name and corresponding coinmarketcap id,
#as defined here: https://api.coinmarketcap.com/v2/listings/
tokensid={'bitcoin':1,
        'bitcoin-cash':1831,
        'ethereum':1027,
        'dash':131,
        'stellar':512,
        'bitcoin-diamond':2222,
        'viuly':2198,
        'xenon':2106}

#this simple class stores the info relevant to each of your coins, 
#used for displaying info in the table
class CoinClass:
    def __init__(self,coin,coinId,amount,usd,eur):
        self.coinName=coin
        self.coinId=coinId
        self.amount=amount
        self.usdPerCoin=usd
        self.eurPerCoin=eur
    def getTotalUSDVal(self):
        return self.amount*self.usdPerCoin
    def getTotalEURVal(self):
        return self.amount*self.eurPerCoin


urlstr1='https://api.coinmarketcap.com/v2/ticker/'
urlstr2='/?convert=EUR'
coindict=tokens.items()
coins=[]
for coin in coindict:
    coinid=tokensid[coin[0]]
    url=urlstr1+str(coinid)+urlstr2
    jsonrsp = requests.get(url)
    #load the list of dictionaries representing the query response
    responsedictionary=json.loads(jsonrsp.content)
    #read the current prices from the dictionary
    usd=float(responsedictionary['data']['quotes']['USD']['price'])
    eur=float(responsedictionary['data']['quotes']['EUR']['price'])
    #append all relevant data from the current coin to the list of coins
    coins.append(CoinClass(coin[0],coinid,coin[1],usd,eur))

#create table
headerCoin=colored("Coin","red",attrs=['bold'])
headerAmount=colored("Amount","yellow",attrs=['bold'])
headerCoinValUSD=colored("USD/coin","green",attrs=['bold'])
headerDollarValue=colored("Total USD","green",attrs=['bold'])
headerCoinValEUR=colored("EUR/coin","blue",attrs=['bold'])
headerEuroValue=colored("Total EUR","blue",attrs=['bold'])

#tabulators
totalUSD=0
totalEUR=0

#Populate the table with data
coinsTableData = []
coinsTableData.append([headerCoin,headerAmount,headerCoinValUSD,
    headerDollarValue,headerCoinValEUR,headerEuroValue])
for coin in coins:
    usd=coin.getTotalUSDVal()
    totalUSD=totalUSD+usd
    eur=coin.getTotalEURVal()
    totalEUR=totalEUR+eur
    coinsTableData.append([coin.coinName,
        ("%12.5f" % coin.amount),
        '${:,.2f}'.format(Decimal(coin.usdPerCoin).quantize(TWOPLACES)),
        '${:,.2f}'.format(Decimal(usd).quantize(TWOPLACES)),
        '€{:,.2f}'.format(Decimal(coin.eurPerCoin).quantize(TWOPLACES)),
        '€{:,.2f}'.format(Decimal(eur).quantize(TWOPLACES))])

coinsTable=SingleTable(coinsTableData)

#Print the table
print(coinsTable.table)

usd_str=colored("USD","green",attrs=['bold'])
eur_str=colored("EUR","blue",attrs=['bold'])
usdv_str=colored('${:,.2f}'.format(Decimal(totalUSD).quantize(TWOPLACES)),"green",attrs=['bold'])
eurv_str=colored('€{:,.2f}'.format(Decimal(totalEUR).quantize(TWOPLACES)),"blue",attrs=['bold'])

#Print Net Value in chosen fiat currencies
print("Total ",usd_str," = ",usdv_str) 
print("Total ",eur_str," = ",eurv_str)
