INTRODUCTION

Simple terminal program to calculate the net value of your cryto assets 
using current values from coinmarketcap.com and display the results
in a colorful table.

INSTALLATION

The networth.py file must be modified for it to be useful to you. Search Tag CUSTOMIZE to 
find the relevant areas

3rd Party libraries are required, and can be installed with

    pip3 install termcolor
    pip3 install terminaltables


USAGE

Run by simply typing networth.py at the command prompt. 
Internet access required.

SCREENSHOT

![Screenshot](output.png)
